package pl.imiajd.Karcz;
import java.util.*;
import  java.lang.CloneNotSupportedException;
import pl.imiajd.Karcz.OsobaL10;
//implements Comparable<StudentL10> ,Cloneable
public class StudentL10 extends OsobaL10 implements Cloneable {
    public StudentL10(String nazwisko,int y,int m,int d,double sr){
        super(nazwisko,y,m,d);
        this.sredniaOcen=sr;
    }

    public int compareTo(OsobaL10 s) {
        if(super.compareTo(s)!=0)return super.compareTo(s);
        else{
                StudentL10 other=(StudentL10)s;
                if (this.getSredniaOcen() > other.getSredniaOcen()) return -1;
                else if (this.getSredniaOcen() < other.getSredniaOcen()) return 1;
                else return 0;
        }
    }

    @Override
    public String toString() {
        return "{"+ super.toString()+
                "sredniaOcen=" + sredniaOcen +
                '}';
    }
    @Override
    public StudentL10 clone() throws  CloneNotSupportedException{
        StudentL10 cloned=(StudentL10)super.clone();
        cloned.setSredniaOcen( this.getSredniaOcen());
        cloned.setDataUrodzenia(this.getDataUrodzenia());
        cloned.setNazwisko(this.getNazwisko());
        return cloned;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    private double sredniaOcen;
}
