package pl.imiajd.Karcz;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class OsobaL10 implements  Comparable<OsobaL10> , Cloneable  {

    public OsobaL10(String nazwisko,int y,int m,int d) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia=LocalDate.of(y,m,d);
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    @Override
    public int compareTo(OsobaL10 o){
        // return this.getDataUrodzenia().compareTo(o.getDataUrodzenia());
        if(o.getNazwisko().compareTo(this.getNazwisko())==0){
            if(o.getDataUrodzenia().compareTo(this.getDataUrodzenia())==0)return 0;
            else if(o.getDataUrodzenia().compareTo(this.getDataUrodzenia())>=1)return 1;
            else  return -1;
        }
        else if(o.getNazwisko().compareTo(this.getNazwisko())>=1)return 1;
        else return -1;

    }
    @Override
    public String toString(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        return "["+this.nazwisko+" "+formatter.format(this.dataUrodzenia)+"]";
    }
    @Override
    public boolean equals(Object otherObject){
        if(this==otherObject)return true;
        if(otherObject==null)return false;
        if(getClass()!=otherObject.getClass())return false;
        if(!(otherObject instanceof OsobaL10))return false;
        OsobaL10 other=(OsobaL10)otherObject;
        if(other.getNazwisko().equals(this.getNazwisko())&& other.getDataUrodzenia().equals(this.getDataUrodzenia())){
            return true;
        }else return false;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}
