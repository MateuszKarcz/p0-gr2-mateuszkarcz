package pl.edu.uwm.wmii.MateuszKarcz.laboratorium11;

public class PairUntilDemo {
    public static void main(String[] args) {
        Pair<String> tester = new Pair<String>("ala", "kot");
        Pair<String> CopiaSwap = PairUntil.PairUntil_swap(tester);
        System.out.println(tester.getFirst()+" "+tester.getSecond());
        System.out.println(CopiaSwap.getFirst()+" "+CopiaSwap.getSecond());
     }
}