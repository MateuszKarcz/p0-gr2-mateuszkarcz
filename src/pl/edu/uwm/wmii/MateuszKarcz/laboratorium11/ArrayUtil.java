package pl.edu.uwm.wmii.MateuszKarcz.laboratorium11;

import java.sql.Array;
import java.time.LocalDate;
import java.util.Arrays;

public class ArrayUtil {
    public static void main(String []args){
        Integer test1[]={
          new Integer(5),
          new Integer(2),
          new Integer( 8),
          new Integer(1),
          new Integer(9),
          new Integer( 7)
        };
        for(int k=0;k<test1.length;k++)System.out.print(test1[k]+" ");
        System.out.println();
        System.out.println("isSorted "+isSorted(test1));
       //selectoinSort(test1);
        test1=margeSort(test1);
        for(int k=0;k<test1.length;k++)System.out.print(test1[k]+" ");
        System.out.println();
        System.out.println("isSorted "+isSorted(test1));
        LocalDate test2[]={
                LocalDate.of(2000,11,1),
                LocalDate.of(2000,1,1),
                LocalDate.of(2001,11,1),
        };
        System.out.println(test2[0]+" "+test2[1]+" "+test2[2]);
        System.out.println("isSorted "+isSorted(test2));
        //selectoinSort(test2);
        margeSort(test2);
        System.out.println(test2[0]+" "+test2[1]+" "+test2[2]);
        System.out.println("isSorted "+isSorted(test1));
        System.out.println("binSearch "+" : "+binSearch(test1,test1[2]));
        System.out.println("binSearch LocalDate"+" : "+binSearch(test2,test2[0]));
    }
    public static<T extends Comparable> boolean isSorted(T[]tab){
        if(tab==null)return false;
        T buf=tab[0];
        for(int i=1;i<tab.length;i++){
            if(buf.compareTo(tab[i])>0)return false;
            buf=tab[i];
        }
        return true;
    }
    public static <T extends Comparable> int binSearch(T []tab,T object){
        if(tab==null || object==null)return -1;
        int pointer=Math.round(tab.length/2);
        int l=0,p=tab.length-1;
        do{
            if(tab[pointer].compareTo(object)==0)return pointer;
            else if(object.compareTo(tab[pointer])>0){
                l=pointer;
                 pointer=(1+pointer+p)/2;
            }
            else {
                p=pointer;
                pointer=(l+pointer)/2;
            }
        }while (l!=p);
        if(tab[0].compareTo(object)==0)return 0;
        return -1;
    }
    public static <T extends Comparable> void selectoinSort(T tab[]){
        for(int i =0;i<tab.length;i++){
            int min=i;
            for(int j=i+1;j<tab.length;j++){
                if(tab[min].compareTo(tab[j])>0)min=j;
            }
            T bug=tab[i];
            tab[i]=tab[min];
            tab[min]=bug;
        }
    }
    public static <T extends Comparable>T[] margeSort(T []tab){
        if(tab.length>1){
            T[]l=margeSort( Arrays.copyOfRange(tab,0,(int)(tab.length/2)));
            T[]p=margeSort( Arrays.copyOfRange(tab,(int)(tab.length/2),tab.length));
            int il=0,ip=0,i=0;
            while(il<l.length&&ip<p.length){
                if(l[il].compareTo(p[ip])<=0){
                    tab[i]=l[il];
                    i++;il++;
                }
                else {
                    tab[i]=p[ip];
                    i++;ip++;
                }
            }
            for(;il<l.length;)tab[i++]=l[il++];
            for(;ip<p.length;)tab[i++]=p[ip++];
            return tab;
        }
        else return tab;
    }
}

