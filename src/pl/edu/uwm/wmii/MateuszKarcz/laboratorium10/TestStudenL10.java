package pl.edu.uwm.wmii.MateuszKarcz.laboratorium10;

import pl.imiajd.Karcz.OsobaL10;
import pl.imiajd.Karcz.StudentL10;
import java.*;
import java.util.ArrayList;
import  java.lang.CloneNotSupportedException;
public class TestStudenL10 {
    public static void main(String[]args){
        ArrayList<StudentL10> grupa=new ArrayList<StudentL10>();
        grupa.add(new StudentL10("Zbigniew",2009,2,21,5.3));
        grupa.add(new StudentL10("Jan",2000,1,11,3.3));
        grupa.add(new StudentL10("Zbigniew",2009,2,21,4.3));
        grupa.add(new StudentL10("Tomek",2009,2,21,5.1));
        grupa.add(new StudentL10("Zbigniew",2009,2,21,5.3));
        grupa.add(new StudentL10("Jan",1999,11,1,2.2));
        grupa.add(new StudentL10("Zbigniew",2009,2,21,5.5));
        for(StudentL10 e:grupa){
            System.out.println(e.toString());
        }
        System.out.println("po sortowaniu");
        grupa.sort(null);
        for(StudentL10 e:grupa){
            System.out.println(e.toString());
        }
        System.out.println("Test clone");

        try {
            StudentL10  c =( (StudentL10) grupa.get(1)).clone();
            System.out.println(grupa.get(1).toString());
            System.out.println(c.toString());
        }catch (CloneNotSupportedException e){
            System.out.println("Test Blond"+e.getMessage());
        }


    }
}
