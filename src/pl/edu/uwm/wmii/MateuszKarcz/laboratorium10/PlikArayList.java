package pl.edu.uwm.wmii.MateuszKarcz.laboratorium10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class PlikArayList {
    public static  void main(String[]args){
        ArrayList<String> zPliku=new ArrayList<String>();
        try {
            String p="p.txt";
            Scanner in = new Scanner(new File("p.txt"));
            while (in.hasNextLine()){
                zPliku.add(in.nextLine());
            }
        }catch (FileNotFoundException e){
            System.out.println("brak takiego pliku "+e.getMessage());
        }
        finally {
            for(String s:zPliku){
                System.out.println(s);
            }
            System.out.println("\npo sortowaniu\n");
            zPliku.sort(null);
            for(String s:zPliku){
                System.out.println(s);
            }
        }
    }
}

