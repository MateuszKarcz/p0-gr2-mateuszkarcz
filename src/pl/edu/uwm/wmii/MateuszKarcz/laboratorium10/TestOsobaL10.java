package pl.edu.uwm.wmii.MateuszKarcz.laboratorium10;

import pl.imiajd.Karcz.OsobaL10;

import java.util.ArrayList;

public class TestOsobaL10 {
    public static void main(String[]args){
        ArrayList<OsobaL10>grupa=new ArrayList<OsobaL10>();
        grupa.add(new OsobaL10("Jan",2000,1,11));
        grupa.add(new OsobaL10("Zbigniew",2009,2,21));
        grupa.add(new OsobaL10("Tomek",2009,2,21));
        grupa.add(new OsobaL10("Zbigniew",2009,2,21));
        grupa.add(new OsobaL10("Jan",1999,11,1));
        for(OsobaL10 e:grupa){
            System.out.println(e.toString());
        }
        System.out.println("po sortowaniu");
        grupa.sort(null);
        for(OsobaL10 e:grupa){
            System.out.println(e.toString());
        }
    }
}
