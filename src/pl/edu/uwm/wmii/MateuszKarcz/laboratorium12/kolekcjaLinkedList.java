package pl.edu.uwm.wmii.MateuszKarcz.laboratorium12;

import java.util.LinkedList;

public class kolekcjaLinkedList {
    public static<T> void  odwroc(LinkedList<T> lista){
        int l=0,p=lista.size()-1;
        while(l<p){
            T buf=lista.get(l);
            lista.set(l,lista.get(p));
            lista.set(p,buf);
            p--;l++;
        }
    }
    public static void redukuj(LinkedList lista,int n){
        int k=0;
        for(int i=0;i<lista.size();i++){
            if(i==k){
                lista.remove(i);
                k+=n-1;
            }
        }
    }
}
