package pl.edu.uwm.wmii.MateuszKarcz.laboratorium12;

import pl.imiajd.Karcz.Srzypce;

import java.util.LinkedList;

public class TestKolLinkedList {
    public static void main(String[]args){
        LinkedList<String>pracownicy=new LinkedList<String>();
        pracownicy.add("Ala");
        pracownicy.add("nikt");
        pracownicy.add("ktostam");
        pracownicy.add("full");
        pracownicy.add("kebs");
        pracownicy.add("koper");
        pracownicy.add("pilka");
        pracownicy.add("stefan");
        pracownicy.add("pies");
        pracownicy.add("kot");

        for(String e:pracownicy){
            System.out.println(e);
        }

        System.out.println("//////////////////redukuj/////////////////////");
        kolekcjaLinkedList.redukuj(pracownicy,3);
        for(String e:pracownicy) {
            System.out.println(e);
        }
        System.out.println("\n//////////////////odwroc/////////////////////");
        kolekcjaLinkedList.odwroc(pracownicy);
        for(String e:pracownicy) {
            System.out.print(e+ " ");
        }
        System.out.println("\n//////////////////test Integer/////////////////////");
            LinkedList<Integer>test2=new LinkedList<Integer>();
            test2.add(new Integer(2));test2.add(new Integer(3));
            test2.add(new Integer(4));test2.add(new Integer(7));
            test2.add(new Integer(8));test2.add(new Integer(9));
            for(Integer d:test2){
                System.out.print(d+" ");
            }

            System.out.println("\n//////////////////redukuj/////////////////////");
            kolekcjaLinkedList.redukuj(test2,2);
            for(Integer d:test2) {
                System.out.print(d + " ");
            }
        System.out.println("\n//////////////////odwroc/////////////////////");
        kolekcjaLinkedList.odwroc(test2);
        for(Integer d:test2) {
            System.out.print(d + " ");
        }

    }
}
