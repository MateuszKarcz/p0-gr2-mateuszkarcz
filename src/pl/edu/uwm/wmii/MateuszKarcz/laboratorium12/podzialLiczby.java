package pl.edu.uwm.wmii.MateuszKarcz.laboratorium12;

import java.util.Scanner;
import java.util.Stack;

public class podzialLiczby {
    public static void main(String[]srgs){
        Scanner s=new Scanner(System.in);
        int n=s.nextInt();
        Stack<Integer>stos=new Stack<Integer>();

        for(int i=10;i<n*10;i*=10){
            stos.push((n%i)/(i/10));
        }
        if(n%10==n&&n==0)   stos.push(n);
        while (!stos.empty()) {
            System.out.print(stos.pop() + " ");
        }
    }
}
