package pl.edu.uwm.wmii.MateuszKarcz.laboratorium12;

import java.util.ArrayList;
import java.util.Scanner;

public class sitoErastorenesa {
    public static void main(String[]args){
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int []przesiane=sito(n+1);
        for(int e:przesiane){
            System.out.print(e+" ");
        }

    }
    public static int[] sito(int n){
        int []tab=new int[n];
        int ile=0;
        for (int i=2;i<n;i++)tab[i]=i;

        for (int i=2;i<n;i++){
            if(tab[i]!=0){
                ile++;
                for (int j=2;j*i<n;j++)tab[i*j]=0;
            }
        }
        int []wynik=new int [ile];
        int k=0;
        for (int i=2;i<n;i++) {
            if (tab[i] != 0) {
               wynik[k++]=i;
            }
        }
        return wynik;
    }
}
