package pl.edu.uwm.wmii.MateuszKarcz.kol2Prubny;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TestKompLap {
    public static void main(String []args){
        ArrayList<Komputer> grupa=new ArrayList<Komputer>();
        grupa.add(new Komputer("dell",2000,11,21));
        grupa.add(new Komputer("Aplle",2000,11,21));
        grupa.add(new Komputer("dell",2000,11,21));
        grupa.add(new Komputer("len",2000,11,21));
        grupa.add(new Komputer("dell",2012,11,21));
        System.out.println("\ngrup przed sorowaniem\n") ;
        for(Komputer k:grupa)System.out.println(k.toString());
        grupa.sort(null);
        System.out.println("\npo sorowaniu\n") ;
        for(Komputer k:grupa)System.out.println(k.toString());

        ArrayList<Laptop> grupaLaptop=new ArrayList<Laptop>();
        grupaLaptop.add(new Laptop("dell",2000,11,21,true));
        grupaLaptop.add(new Laptop("Aplle",2000,11,21,true));
        grupaLaptop.add(new Laptop("dell",2000,11,21,false));
        grupaLaptop.add(new Laptop("len",2000,11,21,false));
        grupaLaptop.add(new Laptop("dell",2012,11,21,true));
        System.out.println("\ngrup przed sorowaniem\n") ;
        for(Laptop k:grupaLaptop)System.out.println(k.toString());
        grupaLaptop.sort(null);
        System.out.println("\npo sorowaniu\n") ;
        for(Laptop k:grupaLaptop)System.out.println(k.toString());
    }
}
