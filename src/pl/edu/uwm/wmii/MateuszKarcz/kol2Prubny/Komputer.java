package pl.edu.uwm.wmii.MateuszKarcz.kol2Prubny;

import java.time.LocalDate;

public class Komputer implements Comparable<Komputer>,Cloneable{
    public Komputer(String n,int y,int m,int d){
        this.nazwa=n;
        this.daraProdukcji=LocalDate.of(y,m,d);
    }
    @Override
    public boolean equals(Object otherObject){
        if(this==otherObject)return true;
        if(otherObject==null)return false;
        if(getClass()!=otherObject.getClass())return false;
        if(!(otherObject instanceof Komputer))return false;
        Komputer other=(Komputer)otherObject;
        if(getNazwa().equals(other.getNazwa())&& getDaraProdukcji().equals(other.getDaraProdukcji())){
            return true;
        }
        else return false;
    }
    @Override
    public int compareTo(Komputer k)throws NullPointerException{
        if(this.getNazwa().compareTo(k.getNazwa())==0){
            return this.getDaraProdukcji().compareTo(k.getDaraProdukcji());
        }else if(this.getNazwa().compareTo(k.getNazwa())>0)return 1;
        else return -1;
    }
    @Override
    public Komputer clone() throws CloneNotSupportedException{
        Komputer cloned=(Komputer)super.clone();
        cloned.setNazwa(this.getNazwa());
        cloned.setDaraProdukcji(this.getDaraProdukcji());
        return cloned;
    }
    @Override
    public String toString(){
        return "["+getNazwa()+" "+getDaraProdukcji()+"]";
    }
    public String getNazwa() {
        return this.nazwa;
    }

    public LocalDate getDaraProdukcji() {
        return this.daraProdukcji;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setDaraProdukcji(LocalDate daraProdukcji) {
        this.daraProdukcji = daraProdukcji;
    }

    private String nazwa;
    private LocalDate daraProdukcji;
}
