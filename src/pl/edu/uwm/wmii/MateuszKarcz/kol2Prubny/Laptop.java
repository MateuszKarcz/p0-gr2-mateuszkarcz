package pl.edu.uwm.wmii.MateuszKarcz.kol2Prubny;

public class Laptop extends Komputer implements Comparable<Komputer>,Cloneable{
    public Laptop (String n,int y,int m,int d,boolean cA){
        super(n,y,m,d);
        this.czyApple=cA;
    }
    @Override
    public int compareTo(Komputer other)throws NullPointerException{
        if(super.compareTo(other)!=0)return super.compareTo(other);
        else{
            Laptop l=(Laptop)other;
            if(this.isCzyApple()==l.isCzyApple())return 0;
            else if(l.isCzyApple()==true)return 1;
            else return -1;
        }
    }
    @Override
    public Laptop clone() throws CloneNotSupportedException{
        Laptop cloned=(Laptop)super.clone();
        cloned.setCzyApple(this.isCzyApple());
        return cloned;
    }
    public boolean isCzyApple() {
        return czyApple;
    }

    public void setCzyApple(boolean czyApple) {
        this.czyApple = czyApple;
    }
    @Override
    public String toString(){
        return super.toString()+"czu Aplle:"+this.isCzyApple();
    }
    private boolean czyApple;
}
