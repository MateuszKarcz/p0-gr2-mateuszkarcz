package pl.edu.uwm.wmii.MateuszKarcz.laboratorium07;
import pl.imiajd.Karcz.Student;
import pl.imiajd.Karcz.Nauczyciel;
import pl.imiajd.Karcz.Osoba;

public class Z4_5TestOsobaStudentNauczyciel {
    public static void main(String[]args){
        Osoba o1=new Osoba("Mateusz",1997);
        System.out.println(o1.getNazwisko()+" "+o1.getRokUrozenia());
        System.out.println(o1.toString());

        Student s1=new Student("Jan",2010,"Fizyka atomowa");
        System.out.println(s1.getNazwisko()+" "+s1.getRokUrozenia()+" "+s1.getKierunek());
        System.out.println(s1.toString());

        Nauczyciel n1=new Nauczyciel("Piotr",2020,4000000);
        System.out.println(n1.getNazwisko()+" "+n1.getRokUrozenia()+" "+n1.getPensja());
        System.out.println(n1.toString());

        Osoba o2=new Student("zgredek",432,"figlei psikusy");
        System.out.println(o2.getNazwisko()+" "+o2.getRokUrozenia()+" "+((Student) o2).getKierunek());
        System.out.println(o2.toString());

        Osoba o3=new Nauczyciel("Jurek",2000,341);
        System.out.println(o3.getNazwisko()+" "+o3.getRokUrozenia()+" "+(((Nauczyciel) o3).getPensja()));
        System.out.println(o3.toString());
    }
}
