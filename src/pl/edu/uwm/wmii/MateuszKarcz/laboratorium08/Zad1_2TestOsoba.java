package pl.edu.uwm.wmii.MateuszKarcz.laboratorium08;
import pl.imiajd.Karcz.Osoba;
import pl.imiajd.Karcz.Student;
import pl.imiajd.Karcz.Pracownik;
public class Zad1_2TestOsoba {
    public static void main(String[] args)
{
    Osoba[] ludzie = new Osoba[2];

    ludzie[0] = new Pracownik(" Kowalski",new String[]{"jan"},true, 1990,11,11,5000, 1999,12,12);
    ludzie[1] = new Student(" Nowak", new String[]{"Malgorzata"},false,1899,1,1,"informatyka",7.64);
    //ludzie[2] = new Osoba("Dyl Sowizdrzał");

    for (Osoba p : ludzie) {
        System.out.println(p.getNazwisko() + ": " + p.getOpis());
    }
}
}
