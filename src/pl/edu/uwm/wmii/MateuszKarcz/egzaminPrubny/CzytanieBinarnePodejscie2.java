package pl.edu.uwm.wmii.MateuszKarcz.egzaminPrubny;
import java.io.*;
public class CzytanieBinarnePodejscie2 {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String fileName ="C:\\Users\\Mateusz\\IdeaProjects\\p0-gr2-mateuszkarcz\\src\\pl\\edu\\uwm\\wmii\\MateuszKarcz\\egzaminPrubny\\testowy.doc";
        File file = new File(fileName);

        try  {
            FileInputStream fileInputStream = new FileInputStream(file);
            int singleCharInt;
            int counter = 0;

            while ((singleCharInt = fileInputStream.read()) != -1) {
                System.out.print((char)singleCharInt);
                counter++;
                if (counter % 64 == 0)
                    System.out.println();
            }
            System.out.println();
        }catch (FileNotFoundException e){}
    }
}
