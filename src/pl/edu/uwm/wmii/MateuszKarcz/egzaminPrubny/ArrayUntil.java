package pl.edu.uwm.wmii.MateuszKarcz.egzaminPrubny;

import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;

public class ArrayUntil {
    public static <T extends Comparable>  boolean isSorted(T []tab){
        if (tab==null)return false;
        for(int i=1;i<tab.length;i++){
            if (tab[i].compareTo(tab[i-1])<0)return false;
        }
        return true;
    }
}
