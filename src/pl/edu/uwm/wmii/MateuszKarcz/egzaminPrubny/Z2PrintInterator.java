package pl.edu.uwm.wmii.MateuszKarcz.egzaminPrubny;

import java.util.Iterator;

public class Z2PrintInterator {
    public static <T extends Iterable> void print(T x){
        Iterator<T> it=x.iterator();
        while (it.hasNext()){
            System.out.print(it.next()+" , ");
        }
    }
}
