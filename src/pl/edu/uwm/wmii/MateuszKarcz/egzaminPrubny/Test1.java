package pl.edu.uwm.wmii.MateuszKarcz.egzaminPrubny;

import java.time.LocalDate;

public class Test1 {
    public static void main(String[]args){
        Integer[] t1={1,2,2,3,4};
        System.out.println(" isSortet:"+ArrayUntil.isSorted(t1));
        LocalDate []t2={
                LocalDate.of(2013,2,23),
                LocalDate.of(2014,2,23),
                LocalDate.of(2014,5,23),
                LocalDate.of(2016,2,23)
        };
        System.out.println(" isSortet:"+ArrayUntil.isSorted(t2));
    }
}
