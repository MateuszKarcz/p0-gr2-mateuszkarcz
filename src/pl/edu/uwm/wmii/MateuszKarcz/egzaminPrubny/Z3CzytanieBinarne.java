package pl.edu.uwm.wmii.MateuszKarcz.egzaminPrubny;

import java.io.*;
import java.util.Scanner;

public class Z3CzytanieBinarne {
    public static void main(String []args) {
        DataInputStream inputStream=null;
        String scierzka;
        Scanner io=new Scanner(System.in);
        scierzka=io.nextLine();
        scierzka="C:\\Users\\Mateusz\\IdeaProjects\\p0-gr2-mateuszkarcz\\src\\pl\\edu\\uwm\\wmii\\MateuszKarcz\\egzaminPrubny\\sample.doc";
        try {
            inputStream  = new DataInputStream(new FileInputStream(scierzka));
            io=new Scanner(new FileInputStream(scierzka));
            int ile=0;
            while (io.hasNextByte()){
              //  System.out.print((char) io.nextByte());
                ile++;
                if(ile==64)System.out.println();
            }

            int singleCharInt;
            int counter = 0;
            try {
                while ((singleCharInt = inputStream.read()) != -1) {
                    System.out.print((char) singleCharInt);
                    counter++;
                    if (counter % 64 == 0)
                        System.out.println();
                }
            }catch (IOException w){}
            
        }catch (FileNotFoundException e){

        }finally {
            if(inputStream!=null){
                try {
                    inputStream.close();
                    io.close();
                }catch (IOException e){}
            }
        }
    }
}
