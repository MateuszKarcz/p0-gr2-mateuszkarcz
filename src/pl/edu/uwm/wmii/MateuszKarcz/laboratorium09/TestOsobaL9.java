package pl.edu.uwm.wmii.MateuszKarcz.laboratorium09;
import pl.imiajd.Karcz.OsobaL9;

import java.time.LocalDate;
import java.util.Arrays;

public class TestOsobaL9 {
    public static void main(String []args) {
        OsobaL9[] grupa = new OsobaL9[5];
        grupa[0] = new OsobaL9("adam", 1999, 11, 11);
        grupa[1] = new OsobaL9("adam", 1999, 11, 11);
        grupa[2] = new OsobaL9("olek", 1999, 11, 11);
        grupa[3] = new OsobaL9("olek", 2000, 11, 11);
        grupa[4] = new OsobaL9("zbigniew", 1800, 1, 11);
        for(OsobaL9 e:grupa){
            System.out.println(e.toString());
        }
        System.out.println("po sortowaniu");
          Arrays.sort(grupa);
        for(OsobaL9 e:grupa){
            System.out.println(e.toString());
        }
        int rowne=0,miejsze=0,wieksze=0;
        System.out.println("rowne wieksze miejsze");
        for(int i=0;i<5;i++){
            for (int k=0;k<5;k++){
                int x=grupa[i].compareTo(grupa[k]);
                if(x==0)rowne++;
                if(x==1)wieksze++;
                if(x==-1)miejsze++;
            }
            System.out.println(grupa[i].toString()+ " "+rowne+" "+wieksze+" "+miejsze);
            rowne=0;
            wieksze=0;
            miejsze=0;
        }
        for(int i=0;i<5;i++){
            System.out.println(grupa[i].toString()+" lat: "+grupa[i].ileLat()+" miesency: "+grupa[i].ileMiesiecy()+" dni: "+grupa[i].ileDni());
        }
    }
}
