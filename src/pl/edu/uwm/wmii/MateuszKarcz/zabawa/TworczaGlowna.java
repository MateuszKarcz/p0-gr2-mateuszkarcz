package pl.edu.uwm.wmii.MateuszKarcz.zabawa;

import java.util.Iterator;
import java.util.Stack;
//implements Comparable<TworczaGlowna>,Cloneable
public class TworczaGlowna<T1 extends Comparable > implements Comparable<TworczaGlowna>,Cloneable {
    public TworczaGlowna(Integer a){
        ile=0;
        max=a;
        zInterable=new Stack<T1>();
    }

    public Integer getIle() {
        return ile;
    }

    public Stack<T1> getzInterable() {
        return zInterable;
    }
    public void addStack(T1 x){
        if(ile<=max){
            ile++;
            this.zInterable.push(x);
        }
    }

    public void setIle(Integer ile) {
        this.ile = ile;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public void setzInterable(Stack<T1> zInterable) {
        this.zInterable = zInterable;
    }

    @Override
    public String toString() {
        return "TworczaGlowna{" +
                "max na Stosie=" + ile +
                ", zInterable=" + zInterable +
                '}';
    }

    public Integer getMax() {
        return max;
    }

    @Override
    public int compareTo(TworczaGlowna other){
           /* while (!(this.zInterable.empty()&&!other.getzInterable().empty()) ){//while wykonujesię zawsze jeden raz
                //System.out.println("a");
               int sp=zInterable.peek().compareTo(other.getzInterable().peek());
                if(sp!=0)return sp;
            }
            if(!this.zInterable.empty())return -1;
            else if(!other.getzInterable().empty())return 1;
            else return 0;*/
           if(max.compareTo(other.getMax())==0){
               if(ile.compareTo(other.getIle())==0)return 0;
                else return ile.compareTo(other.getIle());
           }else return max.compareTo(other.getMax());

        }

    @Override
    public boolean equals(Object otherObject){
        if(this==otherObject)return true;
        if(otherObject==null)return false;
        if(getClass()!=otherObject.getClass())return false;
        if((otherObject instanceof TworczaGlowna))return false;
        TworczaGlowna other=(TworczaGlowna)otherObject;
        if(max.equals(other.max)&&ile.equals(other.ile)&&this.compareTo(other)==0)return true;
        else return false;
    }
    @Override
    public TworczaGlowna clone()throws CloneNotSupportedException{
        TworczaGlowna<T1> cloned= (TworczaGlowna<T1>)super.clone();
        cloned.setIle(this.getIle());
        cloned.setMax(this.getMax());
        cloned.setzInterable(this.getzInterable());
        return cloned;
    }
    private Integer ile;
    private Integer max;
    private Stack<T1> zInterable;
}
