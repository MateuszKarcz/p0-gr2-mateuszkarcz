package pl.edu.uwm.wmii.MateuszKarcz.laboratorium13;

public class Student implements Comparable<Student> {
    public Student(String i,String n){
        imie=i;
        nazwisko=n;
        id=nextId++;
    }

    public String getImie() {
        return imie;
    }

    public static int getNextId() {
        return nextId;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }

    @Override
    public int compareTo(Student other) throws NullPointerException{
        if(this.nazwisko.compareTo(other.getNazwisko())==0){
            if(this.imie.compareTo(other.getImie())==0){
                if(this.id==other.getId())return 0;
                else return this.id-other.getId();
            }
            else return this.imie.compareTo(other.getImie());
        }else return this.nazwisko.compareTo(other.getNazwisko());
    }
    @Override
    public boolean equals(Object otherObject){
        if(otherObject==null)return false;
        if(this==otherObject)return true;
        if(getClass()!=otherObject.getClass())return false;
        if(!(otherObject instanceof Student))return false;
        Student other=(Student)otherObject;
        if(id==other.getId() &&nazwisko.equals(other.getNazwisko())&&imie.equals(other.getImie()))return true;
        else return false;
    }
    private int id;
    private String imie;
    private String nazwisko;
    public static int nextId=0;
}
