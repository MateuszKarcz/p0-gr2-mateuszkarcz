package pl.edu.uwm.wmii.MateuszKarcz.laboratorium13;

import java.util.Iterator;
import java.util.PriorityQueue;

public class kolejkaPiorytetowa {
    public kolejkaPiorytetowa(){
        kol=new PriorityQueue<String>();
    }
    public void dodaj(int waga,String opis){
        this.kol.add((waga+opis));
    }
    public void nastemny(){
        if(kol.iterator().hasNext())kol.remove().charAt(0);
    }

    public PriorityQueue<String> getKol() {
        return kol;
    }

    public static void wyswietl(PriorityQueue<String>k){
        Iterator<String> it=k.iterator();
        while(it.hasNext()){
            String s=it.next();
            System.out.println("waga:"+s.charAt(0)+" Opis: "+s.substring(1,s.length()));
        }
    }
    private PriorityQueue<String>kol;
}
