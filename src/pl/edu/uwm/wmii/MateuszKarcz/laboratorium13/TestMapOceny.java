package pl.edu.uwm.wmii.MateuszKarcz.laboratorium13;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Map;

public class TestMapOceny {
    public static void main(String []args){
        MapaOcen test=new MapaOcen();
        test.dodaj(new Student("Susan","C"), "dbd");
        test.dodaj(new Student("Carl","A"),"db+");
        test.dodaj(new Student("Joe","A"),"db");
        System.out.println(test.toString());
        test.edytuj(2,"Ndst");
        System.out.println(test.toString());
        test.usun(2);
        System.out.println(test.toString());

    }
}
