package pl.edu.uwm.wmii.MateuszKarcz.laboratorium13;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapaOcen {
    public MapaOcen(){
        mapa=new TreeMap<Student, String>();
        mapaSt=new TreeMap<Integer, Student>();
    }
    public void dodaj(Student s,String ocena){
        mapa.put(s,ocena);
        boolean sp=false;
        for(Map.Entry<Integer,Student> e:mapaSt.entrySet()){
            if(e.getKey()==s.getId())sp=true;
        }
        if(sp==false)mapaSt.put(s.getId(),s);
    }
    public void usun(int id){
        mapa.remove(mapaSt.get(id));
    }
    public Student getStudent(int id){
        return mapaSt.get(id);
    }
    public void edytuj(int id,String ocena){
        this.mapa.remove(mapaSt.get(id));
        dodaj(mapaSt.get(id),ocena);
    }
    @Override
    public String toString(){
        String wynik="";
        for(Map.Entry<Student,String> e:mapa.entrySet()){
            wynik+=e.getKey().toString()+": "+e.getValue()+"\n";
        }
        return wynik;
    }
    private Map<Student,String>mapa;
    private Map<Integer,Student>mapaSt;

}
